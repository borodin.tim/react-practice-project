import React, { useState, Fragment } from 'react';

import AddUser from './Components/Users/AddUser';
import UsersList from './Components/Users/UsersList';
import Modal from './Components/UI/Modal';

import Wrapper from './Components/Helpers/Wrappper';

const DUMMY_USERS = [
  { id: 1, username: 'Adam', age: '40' },
  { id: 2, username: 'Bob', age: '29' },
];

function App() {
  const [usersList, setUsersList] = useState('');
  const [showErrorModal, setShowErrorModal] = useState(false);
  const [error, setError] = useState();
  const errorMsgEmptyInput = 'Please enter a valid name and age (non-empty values).';
  const errorMsgBadAge = 'Please enter a valid age (>0).';

  const handleUserAdd = (props) => {
    if (props.user.username.trim().length === 0 || props.user.age.trim().length === 0) {
      setError({
        title: 'Invalid Input',
        message: errorMsgEmptyInput
      });
      setShowErrorModal(true);
      return;
    }

    if (parseInt(props.user.age) < 1) {
      setError({
        title: 'Invalid Age',
        message: errorMsgBadAge
      });
      setShowErrorModal(true);
      return;
    }

    setUsersList((prevState) => [props.user, ...prevState]);
  }

  const handleModalClose = (e) => {
    setShowErrorModal(false);
    setError(null);
  }

  return (
    <Fragment>
      <>
        <Wrapper>
          <div>
            <AddUser onUserAdd={handleUserAdd} />
            <UsersList usersList={usersList} />
            {error && <Modal show={showErrorModal} onClose={handleModalClose} title={error.title} message={error.message} btnText={'Okay'} />}
          </div>
        </Wrapper>
      </>
    </Fragment>
  );
}

export default App;
