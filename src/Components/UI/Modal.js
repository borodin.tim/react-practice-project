import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';

import styles from './Modal.module.css';
import Button from './Button';
import Card from './Card';

const Backdrop = (props) => {
    const showHide = props.show ? `${styles.backdrop} ${styles.displayBlock}` : `${styles.backdrop} ${styles.displayNone}`;
    return <div className={showHide} onClick={props.onClose} />;
};

const ModalOverlay = (props) => {
    return (
        <Card customClassName={styles.modal} onClick={e => e.stopPropagation()}>
            <header className={styles.header}>
                <h2>{props.title}</h2>
            </header>
            {props.message && <p className={styles.content}>{props.message}</p>}
            <footer className={styles.actions}>
                <Button type='button' onClick={props.onClose}>{props.btnText || 'Ok'}</Button>
            </footer>
        </Card>
    );
};

const Modal = (props) => {
    const closeOnEscapeKeyDown = (e) => {
        if ((e.charCode || e.keyCode) === 27) {// 27=escape
            props.onClose();
        }
    }

    useEffect(() => {
        document.body.addEventListener('keydown', closeOnEscapeKeyDown);
        return function cleanup() {
            document.body.removeEventListener('keydown', closeOnEscapeKeyDown);
        };
    }, [])

    return (
        <>
            {ReactDOM.createPortal(
                <Backdrop
                    onClose={props.onClose}
                    show={props.show}
                />,
                document.getElementById('backdrop-root')
            )}
            {ReactDOM.createPortal(
                <ModalOverlay
                    onClose={props.onClose}
                    title={props.title}
                    message={props.message}
                    btnText={props.btnText}
                />,
                document.getElementById('overlay-root')
            )}
        </>
    );
};

export default Modal;