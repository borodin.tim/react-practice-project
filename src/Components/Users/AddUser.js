import React, { useState, useRef } from 'react';

import styles from './AddUser.module.css';
import Card from '../UI/Card';
import Button from '../UI/Button';

const AddUser = (props) => {
    // const [username, setUsername] = useState('');
    // const [age, setAge] = useState('');
    const nameInputRef = useRef();
    const ageInputRef = useRef();

    // const handleUsernameInput = (e) => {
    //     setUsername(e.target.value);
    // };
    // const handleAgeInput = (e) => {
    //     setAge(e.target.value);
    // }

    const handleUserAdd = (e) => {
        e.preventDefault();

        const enteredUsername = nameInputRef.current.value;
        const enteredAge = ageInputRef.current.value;

        props.onUserAdd({ user: { id: Math.random().toString(), username: enteredUsername, age: enteredAge } });
        // setUsername('');
        // setAge('');
        nameInputRef.current.value = '';
        ageInputRef.current.value = '';
    }

    return (
        <Card customClassName={styles.userInput}>
            <div className={styles.usersInput}>
                <form onSubmit={handleUserAdd}>
                    <label htmlFor="username-input">Username</label>
                    <input
                        id="username-input"
                        type="text"
                        // value={username}
                        // onChange={handleUsernameInput}
                        autoComplete="off"
                        ref={nameInputRef}
                    />
                    <label htmlFor="age-input">Age (Years)</label>
                    <input
                        id="age-input"
                        type="number"
                        // value={age}
                        // onChange={handleAgeInput}
                        ref={ageInputRef}
                    />
                    <Button type={'submit'}>Add User</Button>
                </form>
            </div>
        </Card>
    );
};

export default AddUser;