import React from 'react';

import styles from './UserElement.module.css'

const UserElement = ({ user }) => {
    const userData = `${user.username} (${user.age} years old)`;

    return (
        <div className={styles['user-element']}>{userData}</div>
    );
};

export default UserElement;