import React from 'react';

import styles from './UsersList.module.css'
import UserElement from './UserElement';
import Card from '../UI/Card';

const UsersList = (props) => {
    const usersList = props.usersList.length > 0 ? props.usersList.map(user => {
        return (
            <UserElement key={user.id} user={user} />
        )
    })
        :
        <p>No users yet</p>;

    return (
        <Card customClassName={styles.usersList}>
            {usersList}
        </Card>
    );
};

export default UsersList;